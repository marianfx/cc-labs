let config = require('./config');
let util = require('util');

/**
 * Sends data to the client, with a status code, minimum heaers, and stringified JSON data.
 * 
 * @param {any} statusCode 
 * @param {any} response 
 */
function sendData(statusCode, response) {
    var out = JSON.stringify(response, null, 4);
    var length = out.length;

    this.writeHead(statusCode, {
        'Content-Type': "application/json; charset=UTF-8",
        'Content-Length': length
    });
    this.end(out);
}

/**
 * Prepares and sends data to the client in JSON, providing it has success-data present.
 * 
 * @param {Object} toSend Represents a String or a JSON representation of the data to be send to the client.
 */
function sendSuccess(statusCode, toSend) {
    let response = {
        success: 1,
        errors: [],
        data: toSend
    };
    sendData.bind(this)(statusCode, response);
}

/**
 * Prepares and sends data to the client in JSON, alongside an error message and headers.
 * 
 * @param {Object} msg Represents a String or a JSON representation of the data to be send to the client.
 */
function sendError(statusCode, msg) {
    let response = {
        success: 0,
        errors: [{
            message: msg
        }],
        data: {}
    };
    sendData.bind(this)(statusCode, response);
}



/**
 * Prepares and sends data to the client in JSON, alongside 200 OK and Content-Type
 * 
 * @param {Object} toSend Represents a String or a JSON representation of the data to be send to the client.
 */
function send(toSend) {
    sendSuccess.bind(this)(200, toSend);
}

/**
 * Sends the 201 Created response to the client.
 * 
 * @param {Object} toSend Represents a String or a JSON representation of the data to be send to the client.
 */
function send201(toSend) {
    sendSuccess.bind(this)(201, toSend);
}



/**
 * Sends to the client the 404 Bad Request when client does not send good format / syntax.
 * 
 * @param {any} supportedMethodsSeparatedByComma Defaults to the basic methods GET, POST, PUT, DELETE
 */
function send400(msg) {
    msg = "You must send a valid request." + (msg ? msg : "");
    sendError.bind(this)(400, msg);
}


/**
 * Sends to the client the 404 Not Found.
 * 
 * @param {any} msg The error description message
 */
function send404(msg) {
    msg = "You are trying to reach a quite inexistent URI on our servers. Try a GET request on / (root) to see a list of available resources." + (msg ? msg : "");
    sendError.bind(this)(404, msg);
}


/**
 * Sends to the client the 405 Method Not Allowed alongside the supported methods in the headers.
 * 
 * @param {any} supportedMethodsSeparatedByComma Defaults to the basic methods GET, POST, PUT, DELETE
 */
function send405(supportedMethodsSeparatedByComma = "GET, POST, PUT, DELETE") {
    msg = "Method not allowed." + (supportedMethodsSeparatedByComma ? supportedMethodsSeparatedByComma : "");
    sendError.bind(this)(405, msg);

}


/**
 * Sends to the client the 415 Unsupported Format Response when the client sends Unsupported data (not JSON).
 * 
 * @param {any} msg 
 */
function send415(msg) {
    msg = "Unsupported format." + (msg ? msg : "");
    sendError.bind(this)(415, msg);
}



function getRequestParams(str) {
    let queryString = str || '';
    let keyValPairs = [];
    let params = {};

    if (queryString.length) {
        keyValPairs = queryString.split('&');
        for (pairNum in keyValPairs) {
            let key = keyValPairs[pairNum].split('=')[0];
            if (!key.length) continue;
            let val = keyValPairs[pairNum].split('=')[1];
            params[key] = val ? val : {};
        }
    }
    return params;
}

function getRequestBody(req, next) {
    let body = '';
    if (req.method == 'POST' || req.method == 'PUT') {
        req.on('data', (data) => {
            body += data;
        });
        req.on('end', () => {
            next(body);
        });
    } else {
        return next(null);
    }
}

module.exports = {
    send,
    send201,
    send400,
    send404,
    send405,
    send415,
    getRequestParams,
    getRequestBody
};