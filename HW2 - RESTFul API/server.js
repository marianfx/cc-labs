let httpHandler = require('http');
let urlHandler = require('url');
let config = require('./config');
let utils = require('./utils');
let RelData = require('./model/reldata');

class FxServer {

    /**
     * Creates an instance of FxServer. Inits the server, and adds a default handler for root. But does not start the server. Call start() to do that.
     * 
     * @memberOf FxServer
     */
    constructor() {
        console.log("\nServer creation started.");

        this.server = httpHandler.createServer((req, res) => {
            this.setUpRequestAndResponseAndThenHandleThem(this, req, res);
        });

        // keep a list of all base roots (GET + POST)
        this.reldata = new RelData('', '');

        // The Handlers object. At first, the main route sends out the accepted content type.
        this.handlers = {
            'get': [{
                path: '/',
                method: (req, res) => res.send(this.reldata.routes)
            }]
        }
    }

    /**
     * Starts the server. Hostname and port specified in the config file.
     * @memberOf FxServer
     */
    start() {
        this.server.listen(config.port, config.host);
        console.log(`\nListening on http://${config.host}:${config.port}...`);
    }

    
    /**
     * Adds to the response: send, send201, send400, send404, send405, send415
     * Adds to the request: params, body, fxPath, fxId
     * 
     * @param {any} me 
     * @param {any} req 
     * @param {any} res 
     * 
     * @memberOf FxServer
     */
    setUpRequestAndResponseAndThenHandleThem(me, req, res) {
        res.send = utils.send.bind(res);
        res.send201 = utils.send201.bind(res);
        res.send400 = utils.send400.bind(res);
        res.send404 = utils.send404.bind(res);
        res.send405 = utils.send405.bind(res);
        res.send415 = utils.send415.bind(res);

        let urlData = urlHandler.parse(req.url); //get url DATA
        console.log(`\n${req.method} ${urlData.path}`);

        // Step 1 - Get body
        utils.getRequestBody(req, (body) => {
            if (body) {
                try {
                    body = JSON.parse(body);
                } catch (error) {
                    //console.log(error);
                    return res.send415("The server only accepts JSON.");
                }
                req.body = body;
            }

            // Step 2 - Parse call path
            let pathname = urlData.pathname;
            // treat requests to '/' + /users/ => /users
            if (pathname === undefined)
                pathname = "/";
            if (pathname != "/" && pathname.endsWith("/"))
                pathname = pathname.substring(0, pathname.length - 1);

            let regForValidPath = "^(\\/|(\\/\\w+)(\\/(\\d+))?)?$";
            let matches = pathname.match(regForValidPath);
            req.fxPath = matches && matches[2] ? matches[2] : matches && matches[1] && !matches[2] ? matches[1] : ""; //treat special case '/'
            req.fxId = matches && matches[4] ? matches[4] : "";;

            // Step 3 - Get Query Data
            let query = urlData.query;
            req.params = utils.getRequestParams(query);

            // pretty print info
            console.log(`Requested path: '${req.fxPath}'. ${req.fxId != null && req.fxId !== "" ? "Id: '" + req.fxId + "'." : ""}`);
            if (req.body) {
                console.log(`\nReceived data through ${req.method}.`);
                console.dir(req.body);
            }
            console.log(`\nQuery data: ${req.params != null && JSON.stringify(req.params) !== JSON.stringify({})? JSON.stringify(req.params): "No data transmitted as query."}.`);

            // GO NEXT Step
            me.handleRequests(me, req, res);
        });
    }
    
    /**
     * Handles server requests and routes them where they need to go).
     * 
     * @param {http.IncomingMessage} req Represents the RAW request object coming in from the client.
     * @param {http.ServerResponse} res Represents the ServerResponse object.
     * 
     * @memberOf Server
     */
    handleRequests(me, req, res) {
        let subHandlers = [];
        if (req.method == 'GET') {
            subHandlers = me.handlers['get'];
        } else if (req.method == 'POST') {
            subHandlers = me.handlers['post'];
        } else if (req.method == 'PUT') {
            subHandlers = me.handlers['put'];
        } else if (req.method == 'DELETE') {
            subHandlers = me.handlers['delete'];
        } else {
            return res.send405();
        }

        for (var index in subHandlers)
            if (subHandlers[index].path === req.fxPath) {
                console.log(`Found handler for request on ${req.method} ${req.fxPath}.`);
                return subHandlers[index].method(req, res);
            }

        // route not Found
        console.log("No matching route found.");
        res.send404();
    }



    /**
     * Maps the given verb (in GET, POST, PUT, DELETE) to the specified route and will be used to call the respective callback when the route is accessed.
     * 
     * @param {any} verb One of the GET, POST, PUT, DELETE
     * @param {any} route Eg. '/' or '/users'
     * @param {any} cb A Function(req, res) where req is the IncomingMessage and res is the ServerResponse
     * 
     * @memberOf FxServer
     */
    mapVerb(verb, route, cb) {
        if (this.handlers[verb] == null || this.handlers[verb] === undefined)
            this.handlers[verb] = [];

        this.handlers[verb].push({
            path: route,
            method: cb
        });
    }

    /**
     * Catches a GET request on 'route' / route/id and handles it using the logic from 'next'
     * 
     * @param {string} route 
     * @param {ServerCallback} next 
     * 
     * @memberOf Server
     */
    get(route, next) {
        this.mapVerb('get', route, next);
        this.reldata.addRoute(route, 'get');
        this.reldata.addRoute(`${route}/:id`, 'gets');
    }

    /**
     * Catches a POST request on 'route' and handles it using the logic from 'next'
     * 
     * @param {string} route 
     * @param {ServerCallback} next 
     * 
     * @memberOf Server
     */
    post(route, next) {
        this.mapVerb('post', route, next);
        this.reldata.addRoute(route, 'post');
    }

    /**
     * Catches a PUT request on 'route/id' and handles it using the logic from 'next'
     * 
     * @param {string} route 
     * @param {ServerCallback} next 
     * 
     * @memberOf Server
     */
    put(route, next) {
        this.mapVerb('put', route, next);
        this.reldata.addRoute(`${route}/:id`, 'put');
    }

    /**
     * Catches a DELETE request on 'route/id' and handles it using the logic from 'next'
     * 
     * @param {string} route 
     * @param {ServerCallback} next 
     * 
     * @memberOf Server
     */
    delete(route, next) {
        this.mapVerb('delete', route, next);
        this.reldata.addRoute(`${route}/:id`, 'delete');
    }
}

module.exports = FxServer;