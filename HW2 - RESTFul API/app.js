let FxServer = require('./server');
let MovieService = require('./model/movieservice');
let RelData = require('./model/reldata');

let server = new FxServer();
let ms = new MovieService();


server.get("/movies", (req, res) => {
    if(req.fxId)
        return ms.getById(req.fxId, (err, movie) => {
            if(err)
                return res.send404(err);//not found
            movie.links = new RelData('movies', movie.Id).links;
            return res.send(movie);
        });
    
    ms.getAll((err, movies) => {
        for(var index in movies){
            movies[index].links = new RelData('movies', movies[index].Id).links;
        }
        return res.send(movies);
    });
});


server.post("/movies", (req, res) => {
    if(!req.body)
        return res.send400("You must send to the server movie data in JSON format.");
    
    if(req.fxId)
        return res.send400("The request must be on the root of the resources (do not specify an ID).");
    
    ms.create(req.body, (error, movie) => {
        if(error)
            return res.send400(error);
        movie.links = new RelData('movies', movie.Id).links;
        res.send201(movie);
    });
});


server.put("/movies", (req, res) => {
    if(!req.body)
        return res.send400("You must send to the server movie data in JSON format.");

    if(!req.fxId)
        return res.send400("When trying to update a resource, please specify the Id (eg. /movies/1).");
    
    ms.update(req.fxId, req.body, (error, movie) => {
        if(error)
            return res.send404(error);//not found
        movie.links = new RelData('movies', movie.Id).links;
        res.send(movie);
    });
});


server.delete("/movies", (req, res) => {
    if(!req.fxId)
        return res.send400("When trying to delete a resource, please specify the Id (eg. /movies/1).");
    
    ms.delete(req.fxId, (error, movie) => {
        if(error)
            return res.send404(error);//not found
        movie.links = "This resource has been deleted. No URIs available.";
        res.send(movie);
    });
});

server.start();