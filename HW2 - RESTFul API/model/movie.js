class Movie {
    constructor(id, name, desc, reldate) {
        this.Id = id;
        this.Name = name;
        this.Description = desc;
        this.ReleaseDate = reldate;
    }
}

module.exports = Movie;