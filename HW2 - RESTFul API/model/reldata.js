class RelData {
    constructor(name, id) {
        this.links = [{
                "href": `/${name}/${id}`,
                "rel": "self",
                "method": "GET"
            },
            {
                "href": `/${name}/${id}`,
                "rel": "edit",
                "method": "PUT"
            },
            {
                "href": `/${name}/${id}`,
                "rel": "delete",
                "method": "DELETE"
            }
        ];
        this.routes = {
            "version": "1.0",
            "links": []
        };
    }

    getRoutes() {
        return this.routes;
    }

    addRoute(route, method){
        method = method.toUpperCase();
        let obj = {
            "href": route,
            "rel": `${method == 'GET'? 'list' : method == 'GETS' ? 'self' : method == 'POST' ? 'create' : method == 'PUT' ? 'edit' : 'delete'}`,
            "method": method == 'GETS' ? 'GET' : method
        };
        this.routes.links.push(obj);
    }
}

module.exports = RelData;