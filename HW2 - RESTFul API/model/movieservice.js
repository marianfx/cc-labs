let Movie = require('./movie');

class MovieService {
    constructor() {
        this.movies = [];
        this.lastFreeId = 0;
    }

    create(movieData, next) {
        if (!movieData.hasOwnProperty('Name') && !movieData.hasOwnProperty('Description') && !movieData.hasOwnProperty('ReleaseDate'))
            return next("Invalid Movie data sent. Please Specify at least Name, Description or Release Date.");
        if (!movieData.hasOwnProperty('Name'))
            return next("The movie you create must at least have a name.");

        let name = movieData.Name;
        let desc = movieData.hasOwnProperty('Description') ? movieData.Description : "No description available.";
        let date = movieData.hasOwnProperty('ReleaseDate') ? movieData.ReleaseDate : Date.now();
        let movie = new Movie(this.lastFreeId, name, desc, date);
        this.movies.push(movie);
        this.lastFreeId++;
        
        return next(null, movie);
    }

    update(id, movieData, next) {
        let indexToUpdate = null;
        for (var index in this.movies)
            if (this.movies[index].Id == id) {
                indexToUpdate = index;
                break;
            }
        if(!indexToUpdate)
            return next("The requested movie does not exist.");

        let oldData = this.movies[indexToUpdate];
        oldData.Name = movieData.hasOwnProperty('Name') ? movieData.Name : oldData.Name;
        oldData.Description = movieData.hasOwnProperty('Description') ? movieData.Description : oldData.Description;
        oldData.ReleaseDate = movieData.hasOwnProperty('ReleaseDate') ? movieData.ReleaseDate : oldData.ReleaseDate;
        this.movies[indexToUpdate] = oldData;
        return next(null, oldData);
    }

    delete(id, next) {
        let indexToRemove = null;
        for (var index in this.movies)
            if (this.movies[index].Id == id) {
                indexToRemove = index;
                break;
            }
        if(!indexToRemove)
            return next("The requested movie does not exist.");
        
        let movieCopy = this.movies[indexToRemove];
        this.movies.splice(indexToRemove, 1);
        return next(null, movieCopy);
    }

    getAll(next) {
        return next(null, this.movies);
    }

    getById(id, next) {
        for (var index in this.movies)
            if (this.movies[index].Id == id)
                return next(null, this.movies[index]);
        return next("The specified movie cannot be found.");
    }
}

module.exports = MovieService;