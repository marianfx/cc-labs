let comet = require('./comet.io');
let express = require('express');
let ReDbWrapper = require('./rethink');
let _ = require("lodash");


class SocketWrapper {
    constructor() {
        this.io = comet.createServer();
        this.rooms = {}
        this.re = new ReDbWrapper();
    }

    GetComet(){
        return this.io;
    }

    Initialize(app) {
        let me = this;
        app.use(function(req, res, next) {
                var sResult = me.io.serve(req, res);
                console.log(sResult);
                if (!sResult) {
                    var err = new Error('Not Found');
                    err.status = 404;
                    return next(err);
                }
        });

        this.re.Initialize(() => {
            me.io.on('connection', me.onConnecting.bind(me));
            me.re.onChangesFromTable('edit', me.onChangesFromEditTable.bind(me));
        });
    }


    /**
     * Executes when user connects. Binds all the other events to the user's socket.
     * 
     * @param {any} socket The user's socket.
     * 
     * @memberOf SocketWrapper
     */
    onConnecting(socket) {
        console.log("User connected.");
        console.log(socket);
        socket.on('disconnect', this.onDisconnecting.bind(this, socket));
        socket.on('room', this.onTryJoinRoom.bind(this, socket));
        socket.on('acceptroom', this.onJoinRoom.bind(this, socket));
        socket.on('userlistrequest', this.onUpdateUserList.bind(this, socket));
        socket.on('docupdate', this.onDocumentUpdate.bind(this));
    }


    /**
     * Event that catches updates on the client's eeditor and inserts them with RethinkDb
     * 
     * @memberOf SocketWrapper
     */
    onDocumentUpdate(data) {
        console.log("Updating. Got data: " + data.value);
        this.re.InsertIntoTable('edit', data, (err, response) => {});
    }

    /**
     * This is an event handler. it's triggered by ReethinkDb and sends room data to each user in the room.
     * @param {any} err 
     * @param {any} row The row of data (usually the text editor contents)
     * @returns 
     * @memberOf SocketWrapper
     */
    onChangesFromEditTable(err, row) {
        if (err || !row)
            return console.log(err);

        console.log('Sending:' + row.new_val.value);
        _.each(this.rooms[row.new_val.id], (sock) => {
            sock.emit('doc', row);
        });
    }


    /**
     * After the user disconnects, annouce all the users in his room (stored in the db) that he's gone.
     * 
     * @memberOf SocketWrapper
     */
    onDisconnecting(socket) {
        console.log("User disconnected: " + socket._uuid);
        let me = this;
        this.getRoomsForUser(socket._uuid, (err, rooms) => {
            var room = rooms[0];
            console.log(room);
            if (room){
                console.log("Leaving " + room);
                if(me.rooms[room])
                {
                    console.log("Room had " + me.rooms[room].length + " users.");
                    _.remove(me.rooms[room], (sock) => {
                        return sock._uuid == socket._uuid;
                    });
                    console.log("Room has now " + me.rooms[room].length + " users.");
                    _.each(me.rooms[room], (sock) => {
                        sock.emit('memberjoined');
                    });
                }
            }
            else{
                console.log("User was in no rooms.");
            }
        });
    }


    /**
     * Respond to user's request to get list of room's users=
     * @returns 
     * 
     * @memberOf SocketWrapper
     */
    onUpdateUserList(socket, data) {
        console.log("User requesting list of partners.");
        let me = this;
        let sockets = me.rooms[data.room] ? me.rooms[data.room] : [];
        let inRoom = _.find(sockets, (sock) => {
            return sock._uuid === socket._uuid;
        });

        console.log(inRoom ? "User " + data.user + "is in room." : "User " + data.user + " is NOT in room.");
        // if the user is not allowed to see users, send him a list with only him (or if he does not provide his identity, send him 'not allowed')
        if (!inRoom)
        {
            socket.emit('getusers', [data.user ? data.user : 'Not allowed to see users.']);
            return;
        }
        
        // set up method that gets the users for this room and then sends them
        this.getUsersForRoom(data, me.rooms[data.room], true, (err, users) => {
            if (!err) {
                console.log("Sending userlist to user.");
                console.log(users);
                
                socket.emit('getusers', users);
                // _.each(me.rooms[data.room], (sock) => {
                //     sock.emit('getusers', users);
                // });
            }
        });
    }


    /**
     * User's attempt to connect to a room triggers: check if room if empty; if so, try joining / creating, otherwise, send accept request to other active users.
     * @returns 
     * 
     * @memberOf SocketWrapper
     */
    onTryJoinRoom(socket, data) {
        // first step in joining is being approved by another live user
        // if no live users exists, one cannot write but in a personal room
        let me = this;
        console.log(data.user + " trying joining room " + data.room);
        var sockets = me.rooms[data.room] ? me.rooms[data.room] : [];
        //console.log(sockets); 
        let nr = sockets.length;
        console.log("Found " + nr +  "  sockets in room " + data.room + ".");
        
        // case 1 - no users online
        if (nr <= 0) {
            console.log("Room has no users live. Try Joining existing user.");
            this.getUsersForRoom(data, sockets, false, (err, usrs) => {
                if (usrs.length != 0 && usrs.indexOf(data.user) < 0)
                    return socket.emit("failedjoin", "The room you are trying to access exists, and you have no access on it. One of it's users should be online to grant you access. 'Till then access <a href=\"/\">this</a> and join or create another room.");

                me.onJoinRoom(socket, {
                    user: data.user,
                    room: data.room,
                    usersocket: socket._uuid
                });
            });
        }
        else {
            // case 2 - there are users online
            this.getUsersForRoom(data, sockets, true, (err, usrs) => {
                if (usrs.indexOf(data.user) >= 0)
                    return socket.emit("failedjoin", "There is another user with your name logged in. Access <a href=\"/?room=" + data.room + "\">this</a> and join with another name.");

                // get registered users for this room
                console.log("Sending 'acceptme' signal.");
                _.each(sockets, (sock) => {
                    sock.emit('usertriestoenter', {
                        user: data.user,
                        room: data.room,
                        usersocket: socket._uuid
                    });
                });
            });
        }
    }


    /**
     * Triggers when an user joins a room.
     * @returns 
     * 
     * @memberOf SocketWrapper
     */
    onJoinRoom(socket, data) {
        let me = this;
        console.log(data.user + " IS joining room " + data.room);
        let sockets = me.rooms[data.room] ? me.rooms[data.room] : [];
        let inRoom = _.find(sockets, (sock) => {
            return sock._uuid === data.usersocket;
        });
        // join only if still connected
        if (inRoom)
        {
            console.log("User was already in room.");
            return;
        }

        //add to room
        if(!me.rooms[data.room])
            me.rooms[data.room] = [];
        
        if(me.io._sockets[data.usersocket])
            me.rooms[data.room].push(me.io._sockets[data.usersocket]);

        //announce all
        _.each(me.rooms[data.room], (sock) => {
            sock.emit('memberjoined');
        });
        console.log("Continue joining...");

        // if got here, I have aproval from others; if i am not in Db, insert myself
        this.getUsersForRoom(data, sockets, true, (err, usrs) => {
            //console.log("Users on Joining for room " + data.room + ":");
            //console.log(usrs);
            let isOld = usrs.indexOf(data.user) >= 0;

            let sockets = me.rooms[data.room] ? me.rooms[data.room] : [];
            // set up method that gets the users for this room and thensends them
            let getUsersForRoomMethod = me.getUsersForRoom.bind(me, data, sockets, true, (err, users) => {
                if (!err) {
                    console.log("Sending users to users.");
                    console.log(users);
                    _.each(me.rooms[data.room], (sock) => {
                        sock.emit('memberjoined');
                    });
                }
            });

            // set up method that gets room data(text), sends it to user and then calls the method above (which sends users for that room updated)
            let sendRoomDataMethod = me.sendRoomDataToUser.bind(me, data, getUsersForRoomMethod);

            // insert him in Db if not existent (and then update data)
            if (!isOld) {
                return me.insertUserInDb(data, (e, r) => {
                    sendRoomDataMethod();
                });
            }
            // otherwise just update data
            return sendRoomDataMethod();
        });
    }


    /**
     * Emits the socket event which sends the data (editor values) to the user, for the current room.
     * @param {any} data must contain user id and room id
     * @param {any} next 
     * 
     * @memberOf SocketWrapper
     */
    sendRoomDataToUser(data, next) {
        let me = this;
        this.getRoomData(data, (err, result) => {
            let output = result ? result.value : "";
            if (err) {
                console.log("Error happened" + err);
                output = 'Cannot retrieve data for the current room.';
            }

            
            if(me.io._sockets[data.usersocket]){
                console.log("User is OK, i send him ViewData now.");
                me.io._sockets[data.usersocket].emit('canviewserverdata', output)
                return next();
            }
        })
    }

    /**
     * Returns an array of users. If onlyConnected is true, returns only the ones of which socket's live.
     * 
     * @param {Object} data {user, room, usersocket} 
     * @param {Object} sockets {id: value} 
     * @param {Boolean} onlyConnected Specifies if I should return only the connected users or not 
     * @param {any} next Function(users)
     * 
     * @memberOf SocketWrapper
     */
    getUsersForRoom(data, sockets, onlyConnected, next) {
        let getUsersMethod = this.re.GetPlayersForRoom
            .bind(this.re, data.room,
                (err, result) => {

                    if (err) {
                        console.log(err); // do not update, keep prev userlist
                        return next(err);
                    }

                    // build the data containing usernames only
                    var userNames = [];
                    for (var i in result)
                        if (!onlyConnected)
                            userNames.push(result[i].user);
                        else 
                        {
                            let isIn = _.find(sockets, (sock) => {
                                return sock._uuid === result[i].usersocket;
                            });
                            if(isIn)
                                userNames.push(result[i].user);
                        }
                    //console.log(userNames);
                    return next(null, userNames ? userNames : []);
                });
        this.ExecuteDbOp(getUsersMethod);
    }


    /**
     * Returns a list of rooms the given socketid is in (contacts the Db)
     * 
     * @param {any} socketid User's socket id
     * @param {any} next Function(err, rooms)
     * 
     * @memberOf SocketWrapper
     */
    getRoomsForUser(socketid, next) {
        let getUsersMethod = this.re.GetPlayersForSocketId
            .bind(this.re, socketid,
                (err, result) => {

                    if (err) {
                        console.log(err); // do not update, keep prev userlist
                        return next(err);
                    }

                    // emit the data to the user
                    var rooms = [];
                    for (var i in result)
                        rooms.push(result[i].room);
                    //console.log(userNames);
                    return next(null, rooms ? rooms : []);
                });
        this.ExecuteDbOp(getUsersMethod);
    }


    /**
     * Given the reMethod with no parameters, executed it using a new RethinkDb Wrapper (for parallel connections)
     * @memberOf SocketWrapper
     */
    ExecuteDbOp(reMethod) {
        let r = new ReDbWrapper();
        let me = this;
        r.Initialize(reMethod);
    }

    insertUserInDb(data, next) {
        let insertMethod = this.re.InsertIntoTable.bind(this.re, 'rooms', {
            room: data.room,
            user: data.user,
            usersocket: data.usersocket
        }, next);

        this.ExecuteDbOp(insertMethod);
    }

    getRoomData(data, next) {
        let me = this;
        let sendDataMethod = this.re.GetRoomData
            .bind(this.re, data.room,
                next);
        this.ExecuteDbOp(sendDataMethod);
    }
}

module.exports = SocketWrapper;