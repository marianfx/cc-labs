let express = require('express');
let ReDbWrapper = require('../logic/rethink');


class MainRouter {
  constructor() {
    this.router = express.Router();
    this.router.get('/', this.renderRooms);
    this.router.get('/rooms/random/:user', this.getRandomRoom);
    this.router.get('/rooms/:id/:user', this.sendRoom);
    // this.router.get('/rooms/:user/:id', this.sendRoom);
    // this.router.get('/rooms/:user/:id', this.getRoomData);
  }

  // get an instance of this router
  getRouter() {
    return this.router;
  }

  renderRooms(req, res) {
    return res.render('rooms');
  }

  // GET rooms/user/id
  sendRoom(req, res) {

    return res.render('room', {
      data: {
        id: req.params.id,
        user: req.params.user
      }
    });
  }

  getRandomRoom(req, res) {
    let r = new ReDbWrapper();
    let user = req.params.user;
    r.Initialize(() => {
      r.InsertIntoTable('rooms', {
        user: user,
        usersocket: ""
      }, (err, resp) => {
          if(err || !resp.generated_keys)
            return res.redirect('/');
          return res.redirect('/rooms/' + resp.generated_keys + "/" + user);
      });
    });
  }
}

module.exports = MainRouter;