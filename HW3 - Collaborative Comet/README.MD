Install guid:
1. Make sure you have Node installed. If so, run 'npm install' to install Node dependencies
2. Make sure you have Bower installed. If so, run bower install to install Client dependencies
3. Make sure you download and start a RethinkDb Instance
4. Run 'npm start' to start the application