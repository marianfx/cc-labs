var user = $('#user').text();
var room = $('#room').text();

var socket = comet.connect();
console.log("Comet connected.");

$(document)
    .ready(function () {

        $("#copyBtn").click(function () {
            var url = window.location.origin + "/?room=" + encodeURIComponent(room);
            copyTextToClipboard(url);
        });

        // init sidenav
        $('#collapse-users').sideNav({
            menuWidth: 300, // Default is 300
            edge: 'left', // Choose the horizontal origin
            closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
            draggable: true // Choose whether you can drag to open on touch screens
        });
    });


var simplemde = new SimpleMDE({
    element: $("#editor")[0]
});

// on connection, make him try to join the room
socket.on('connect', function () {
    // Connected, let's sign-up for to receive messages for this room
    console.log("Connected.");
    socket.emit('room', {
        user: user,
        room: room
    });
});

// receive updates from users (not from current user to skip causing glitchez)
socket.on('doc', function (row) {
    console.log(row);
    if (row.new_val.user != user) {
        var current_pos = simplemde.codemirror.getCursor();
        simplemde.value(row.new_val.value);
        simplemde.codemirror.setCursor(current_pos);
    }
});

// ask user to confirm other user integration
socket.on('usertriestoenter', function (data) {
    console.log("User " + data.user + " tries to enter room " + data.room);
    console.log(data);
    swal({
            title: data.user + " is tryin' hard to access this room.",
            text: "Do you want to let " + data.user + " in? Anybody in the room can do that. Treat with care.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Accept " + data.user + "."
        },
        function () { // if user accepts the other user, emit the event (to announce the others this one accepted him).
            swal("Accepted!", data.user + " is now part of the group.", "success");
            socket.emit('acceptroom', {
                user: data.user,
                room: room,
                usersocket: data.usersocket
            });
        });
});

// close swal when others accept new member OR when users disconnect
// also re-request userlist
socket.on('memberjoined', function () {
    console.log("User accepted by somebody else or disconnected. Need to update.");
    swal.close();
    socket.emit('userlistrequest', {user: user, room: room});
});

// executes for the user when he's accepted by others
// vasically hands him over access to room data
socket.on('canviewserverdata', function (data) {
    console.log("I  have been accepted");
    $('#acceptme').attr('class', 'hidden');
    $('#welcomemsg').attr('class', 'cuser');
    simplemde.value(data);
});

// display message from server when the users fails to get accepted
socket.on('failedjoin', function (msg) {
    console.log("I  have been accepted");
    $('#acceptme').attr('class', 'username');
    $('#acceptme').html(msg);
    $('#welcomemsg').attr('class', 'hidden');
    simplemde.value(data);
});

// triggered by the server (hands over list of users)
socket.on('getusers', function (users) {
    console.log("Received users.");
    var header = '<li><a href="#!"><i class="material-icons">info</i><b>Room #' + room +'</b></a></li><li></br></li>';
    $('#slide-out').html(header);
    for(index in users){
        var user = '<li><a href="#!"><i class="material-icons">perm_identity</i>' + users[index] + '</a></li>';
        $('#slide-out').append(user);
    }
});

// events on MD editor to send data (the last one triggers when modifyin style)
simplemde.codemirror.on("keyup", sendData);
simplemde.codemirror.on("cut", sendData);
simplemde.codemirror.on("paste", sendData);
$('.editor-toolbar a').click(sendData);

function sendData() {
    var data = {
        id: room,
        user: user,
        value: simplemde.value()
    };
    console.log(data);
    socket.emit('docupdate', data);
}

// http://stackoverflow.com/a/30810322
function copyTextToClipboard(text) {
    var textArea = document.createElement("textarea");

    // Place in top-left corner of screen regardless of scroll position.
    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;

    // Ensure it has a small width and height. Setting to 1px / 1em
    // doesn't work as this gives a negative w/h on some browsers.
    textArea.style.width = '2em';
    textArea.style.height = '2em';

    // We don't need padding, reducing the size if it does flash render.
    textArea.style.padding = 0;

    // Clean up any borders.
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';

    // Avoid flash of white box if rendered for any reason.
    textArea.style.background = 'transparent';

    textArea.value = text;

    document.body.appendChild(textArea);

    textArea.select();

    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
    } catch (err) {
        console.log('Oops, unable to copy');
    }

    document.body.removeChild(textArea);
}