let r = require('rethinkdb');
let config = require('../config');

class ReDbWrapper {
    constructor() {
        this.conn = null;
    }

    Initialize(next) {
        let me = this;
        this.Connect((conn) => {
            me.conn = conn;
            // r.db('test').tableDrop('rooms').run(me.conn, (err, res) => {
            //     me.CreateTable.bind(me, 'edit')(me.CreateTable.bind(me, 'rooms', next));
            // });
           me.CreateTable.bind(me, 'edit')(me.CreateTable.bind(me, 'rooms', next));// calls create table for 'edit' which calls create table for 'users' which calls next()
        });
    }

    Connect(next) {
        r.connect(config.rethingConfig, (err, conn) => {
            if (err) return console.log(err);
            next(conn);
        });
    }

    CreateTable(name, next) {
        r.db('test').tableList().run(this.conn, (err, response) => {
            if (response.indexOf(name) >= 0) {
                //console.log('Table \'' + name + '\' exists. All tables: ' + response);
                return next();
            }
            console.log('Table \'' + name + '\' does not exist. Creating.');
            r.db('test').tableCreate(name).run(this.conn, () => {
                return next();
            });
        });
    }

    GetRoomData(id, next) {
        r.table('edit').get(id).run(this.conn, (err, result) => {
            return next(err, result);
        })
    }

    DeletePlayersFromRoom(id, next){
        r.table('rooms').filter({room: id}).delete().run(this.conn, (err, result) => {
            next();
        });
    }

    GetPlayersForRoom(id, next){
        r.table('rooms').filter({room: id}).run(this.conn, (err, result) => {
            if(err) return next(err);
            result.toArray((er, results) => {
                return next(er, results);
            });
        });
    }

    GetPlayersForSocketId(id, next){
        r.table('rooms').filter({usersocket: id}).run(this.conn, (err, result) => {
            if(err) return next(err);
            result.toArray((er, results) => {
                return next(er, results);
            });
        });
    }

    InsertIntoTable(name, data, next) {//conflict:update = inserts/updates eexisting
        r.table(name).insert(data, {
            conflict: 'update'
        }).run(this.conn, (err, response) => {
            if (err) return next(err);
            console.log("Success inserting.");
            //console.log(response);
            return next(err, response);
        })
    }

    onChangesFromTable(name, next) {
        r.table(name).changes().run(this.conn, (err, cursor) => {
            if (err) return next(err);

            cursor.each((err, row) => {
                next(err, row);
            });
        });
    }
}

module.exports = ReDbWrapper;