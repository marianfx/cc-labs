let socket = require('socket.io');
let ReDbWrapper = require('./rethink');


class SocketWrapper {
    constructor(server) {
        this.io = socket(server);
        this.re = new ReDbWrapper();
        this.Initialize();
    }

    Initialize() {
        let me = this;
        this.re.Initialize(() => {
            me.io.on('connection', me.onConnecting.bind(me));
            me.re.onChangesFromTable('edit', me.onChangesFromEditTable.bind(me));
        });
    }


    /**
     * Executes when user connects. Binds all the other events to the user's socket.
     * 
     * @param {any} socket The user's socket.
     * 
     * @memberOf SocketWrapper
     */
    onConnecting(socket) {
        console.log("User connected.");
        socket.on('disconnect', this.onDisconnecting.bind(this, socket));
        socket.on('room', this.onTryJoinRoom.bind(this, socket));
        socket.on('acceptroom', this.onJoinRoom.bind(this, socket));
        socket.on('userlistrequest', this.onUpdateUserList.bind(this, socket));
        socket.on('docupdate', this.onDocumentUpdate.bind(this));
    }


    /**
     * Event that catches updates on the client's eeditor and inserts them with RethinkDb
     * 
     * @memberOf SocketWrapper
     */
    onDocumentUpdate(data) {
        console.log("Updating. Got data: " + data.value);
        this.re.InsertIntoTable('edit', data, (err, response) => {});
    }

    /**
     * This is an event handler. it's triggered by ReethinkDb and sends room data to each user in the room.
     * @param {any} err 
     * @param {any} row The row of data (usually the text editor contents)
     * @returns 
     * @memberOf SocketWrapper
     */
    onChangesFromEditTable(err, row) {
        if (err || !row)
            return console.log(err);
        console.log('Sending:' + row.new_val.value);
        this.io.sockets.in(row.new_val.id).emit('doc', row);
    }


    /**
     * After the user disconnects, annouce all the users in his room (stored in the db) that he's gone.
     * 
     * @memberOf SocketWrapper
     */
    onDisconnecting(socket) {
        console.log("User disconnected.");
        let me = this;
        this.getRoomsForUser(socket.id, (err, rooms) => {
            var room = rooms[0];
            console.log(room);
            if (room) me.io.sockets.in(room).emit('memberjoined');
        });
    }


    /**
     * Respond to user's request to get list of room's users=
     * @returns 
     * 
     * @memberOf SocketWrapper
     */
    onUpdateUserList(socket, data) {
        console.log("User requesting list of partners.");
        var room = this.io.sockets.adapter.rooms[data.room];
        var sockets = room ? room.sockets : {};

        // if the user is not allowed to see users, send him a list with only him (or if he does not provide his identity, send him 'not allowed')
        if (!socket.id in sockets)
            return me.io.sockets.in(data.room).emit('getusers', [data.user ? data.user : 'Not allowed to see users.']);
        // set up method that gets the users for this room and then sends them
        let me = this;
        this.getUsersForRoom(data, sockets, true, (err, users) => {
            if (!err) {
                console.log("Sending userlist to user.");
                console.log(users);
                me.io.sockets.in(data.room).emit('getusers', users);
            }
        });
    }


    /**
     * User's attempt to connect to a room triggers: check if room if empty; if so, try joining / creating, otherwise, send accept request to other active users.
     * @returns 
     * 
     * @memberOf SocketWrapper
     */
    onTryJoinRoom(socket, data) {
        // first step in joining is being approved by another live user
        // if no live users exists, one cannot write but in a personal room
        console.log(data.user + " trying joining room " + data.room);
        var room = this.io.sockets.adapter.rooms[data.room];
        var sockets = room ? room.sockets : {};
        //console.log(sockets); 
        let me = this;
        let nr = Object.keys(sockets).length;
        //console.log(nr);
        
        // case 1 - no users online
        if (nr <= 0) {
            console.log("Room has no users live. Try Joining existing user.");
            this.getUsersForRoom(data, sockets, false, (err, usrs) => {
                if (usrs.length != 0 && usrs.indexOf(data.user) < 0)
                    return me.io.to(socket.id).emit("failedjoin", "The room you are trying to access exists, and you have no access on it. One of it's users should be online to grant you access. 'Till then access <a href=\"/\">this</a> and join or create another room.");

                me.onJoinRoom(socket, {
                    user: data.user,
                    room: data.room,
                    usersocket: socket.id
                });
            });
        }

        // case 2 - there are users online
        this.getUsersForRoom(data, sockets, true, (err, usrs) => {
            if (usrs.indexOf(data.user) >= 0)
                return me.io.to(socket.id).emit("failedjoin", "There is another user with your name logged in. Access <a href=\"/?room=" + data.room + "\">this</a> and join with another name.");

            // get registered users for this room
            console.log("Sending 'acceptme' signal.");
            me.io.sockets.in(data.room).emit('usertriestoenter', {
                user: data.user,
                room: data.room,
                usersocket: socket.id
            });
        });

    }


    /**
     * Triggers when an user joins a room.
     * @returns 
     * 
     * @memberOf SocketWrapper
     */
    onJoinRoom(socket, data) {
        var room = this.io.sockets.adapter.rooms[data.room];
        var sockets = room ? room.sockets : {};
        var inRoom = data.usersocket in sockets;

        // join only if still connected
        if (!this.io.sockets.connected[data.usersocket] || inRoom)
            return;


        // announce the others
        this.io.sockets.connected[data.usersocket].join(data.room);
        this.io.sockets.in(data.room).emit('memberjoined');
        console.log(data.user + " joining room " + data.room + ".");

        // if got here, I have aproval from others; if i am not in Db, insert myself
        let me = this;
        this.getUsersForRoom(data, sockets, true, (err, usrs) => {
            //console.log("Users on Joining for room " + data.room + ":");
            //console.log(usrs);
            let isOld = usrs.indexOf(data.user) >= 0;

            var room = this.io.sockets.adapter.rooms[data.room];
            var sockets = room ? room.sockets : {};
            // set up method that gets the users for this room and thensends them
            let getUsersForRoomMethod = me.getUsersForRoom.bind(me, data, sockets, true, (err, users) => {
                if (!err) {
                    console.log("Sending users to users.");
                    console.log(users);
                    me.io.sockets.in(data.room).emit('getusers', users);
                }
            });

            // set up method that gets room data(text), sends it to user and then calls the method above (which sends users for that room updated)
            let sendRoomDataMethod = me.sendRoomDataToUser.bind(me, data, getUsersForRoomMethod);

            // insert him in Db if not existent (and then update data)
            if (!isOld) {
                return me.insertUserInDb(data, (e, r) => {
                    sendRoomDataMethod();
                });
            }
            // otherwise just update data
            return sendRoomDataMethod();
        });
    }


    /**
     * Emits the socket event which sends the data (editor values) to the user, for the current room.
     * @param {any} data must contain user id and room id
     * @param {any} next 
     * 
     * @memberOf SocketWrapper
     */
    sendRoomDataToUser(data, next) {
        let me = this;
        this.getRoomData(data, (err, result) => {
            if (err) {
                console.log(err);
                me.io.to(data.usersocket).emit('canviewserverdata', 'Cannot retrieve data for the current room.')
                return next();
            }

            // emit the data to the user
            me.io.to(data.usersocket).emit('canviewserverdata', result ? result.value : "");
            return next();
        })
    }

    /**
     * Returns an array of users. If onlyConnected is true, returns only the ones of which socket's live.
     * 
     * @param {Object} data {user, room, usersocket} 
     * @param {Object} sockets {id: value} 
     * @param {Boolean} onlyConnected Specifies if I should return only the connected users or not 
     * @param {any} next Function(users)
     * 
     * @memberOf SocketWrapper
     */
    getUsersForRoom(data, sockets, onlyConnected, next) {
        let getUsersMethod = this.re.GetPlayersForRoom
            .bind(this.re, data.room,
                (err, result) => {

                    if (err) {
                        console.log(err); // do not update, keep prev userlist
                        return next(err);
                    }

                    // build the data containing usernames only
                    var userNames = [];
                    for (var i in result)
                        if (!onlyConnected)
                            userNames.push(result[i].user);
                        else if (result[i].usersocket in sockets)
                        userNames.push(result[i].user);
                    //console.log(userNames);
                    return next(null, userNames ? userNames : []);
                });
        this.ExecuteDbOp(getUsersMethod);
    }


    /**
     * Returns a list of rooms the given socketid is in (contacts the Db)
     * 
     * @param {any} socketid User's socket id
     * @param {any} next Function(err, rooms)
     * 
     * @memberOf SocketWrapper
     */
    getRoomsForUser(socketid, next) {
        let getUsersMethod = this.re.GetPlayersForSocketId
            .bind(this.re, socketid,
                (err, result) => {

                    if (err) {
                        console.log(err); // do not update, keep prev userlist
                        return next(err);
                    }

                    // emit the data to the user
                    var rooms = [];
                    for (var i in result)
                        rooms.push(result[i].room);
                    //console.log(userNames);
                    return next(null, rooms ? rooms : []);
                });
        this.ExecuteDbOp(getUsersMethod);
    }


    /**
     * Given the reMethod with no parameters, executed it using a new RethinkDb Wrapper (for parallel connections)
     * @memberOf SocketWrapper
     */
    ExecuteDbOp(reMethod) {
        let r = new ReDbWrapper();
        let me = this;
        r.Initialize(reMethod);
    }

    insertUserInDb(data, next) {
        let insertMethod = this.re.InsertIntoTable.bind(this.re, 'rooms', {
            room: data.room,
            user: data.user,
            usersocket: data.usersocket
        }, next);

        this.ExecuteDbOp(insertMethod);
    }

    getRoomData(data, next) {
        let me = this;
        let sendDataMethod = this.re.GetRoomData
            .bind(this.re, data.room,
                next);
        this.ExecuteDbOp(sendDataMethod);
    }
}

module.exports = SocketWrapper;