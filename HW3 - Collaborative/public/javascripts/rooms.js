$(document)
	.ready(function () {

		checkAndFillParams();
		Validation();

		$("#submitBtn")
			.click(function () {

				var isValid = $('#loginForm')
					.valid();

				if (isValid)
					callBackend();
			});
		
		$("#rndmBtn")
			.click(function () {

				var isValid = $('#loginForm')
					.valid();

				if (isValid)
					callBackendRandom();
			});
	});

function callBackend() {
	var _username = $('#userTB').val();
	var _roomId = $('#passTB').val();

	window.location.href = '/rooms/' + _roomId + '/' + _username;
}

function callBackendRandom() {
	var _username = $('#userTB').val();

	window.location.href = '/rooms/random/' + _username;
}

function Validation() {
	$('#loginForm')
		.validate({
			rules: {
				userTB: {
					required: true,
					minlength: 2,
					maxlength: 50
				},
				passTB: {
					required: true
				}
			},
			messages: {
				userTB: {
					required: "*Please fill in your user name.",
					minlength: "*Your username must have at least 2 characters.",
					maxlength: "*Your username must have at most 50 characters."
				},
				passTB: {
					required: "*Please enter a room."
				}
			},
			validClass: 'valid',
			errorClass: 'invalid error',
			errorElement: 'div',
			errorPlacement: function (error, element) {
				var placement = $(element)
					.data('error');
				if (placement) {
					$(placement)
						.append(error);
				} else {
					error.insertAfter(element);
				}
			}
		});
}

// if param for the room exist in the link, fill them (redirrecting on link)
function checkAndFillParams() {
	var href = window.location.href;
	console.log(href);
	var arr = href.split("?");
	if (arr.length < 2)
		return;
	var possible = arr[1];
	arr = possible.split("=");
	if (arr.length < 2)
		return;
	var name = arr[0];
	var value = arr[1];
	if (name !== "room")
		return;
	$('#passTB').val(decodeURIComponent(value));
}