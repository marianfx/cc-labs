let _ = require("lodash");
let config = require('../config');

function treatYouTubeSearch(me, req, res) {
    if (!req.query["q"]) {
        return res.send({
            data: "Error: You must specify the parameter for the search query (q).",
            status: 0
        });
    }
    if (req.query["count"]) {
        let count = req.query["count"].toString()
        count = count.replace("\"", "");
        console.log("Count: " + count);
        if(count !== "0" && count !== "NaN")
            me.youtube.numberOfResults = parseInt(count);
    }
    let searchString = req.query["q"];

    //console.log();
    let message = "Searching YT for query '" + searchString + "', for '" + me.youtube.numberOfResults.toString() + "' max results.";
    console.log(message);
    me.youtube.setKey(config.youtubeKey);
    me.youtube.search(searchString, function (error, result) {
        if (error) {
            console.log(error);
            let errMsg = _.has(error, "message")? error.message: error.toString();
            return res.send({
                data: "Error. Details: " + errMsg,
                status: 0
            });
        }
        console.log("parsing results");
        return me.youtube.parseResults(result, function (parsed) {
            console.log(parsed);
            return res.send(parsed);
        });
    });
}

class YRouter {

    /**
     * Creates an instance of YRouter.
     * @param {express} ex The express app. 
     */
    constructor(ex) {
        this.router = ex.Router();
        let YouTuber = require('../services/youtube');
        this.youtube = new YouTuber();
    }


    /**
     * @returns {Router}
     */
    buildRouter() {
        let me = this;
        this.router.get("/", function(req, res){
            treatYouTubeSearch(me, req, res);
        });

        return this.router;
    }
}

module.exports = YRouter;