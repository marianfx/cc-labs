let YouTube = require('youtube-node');
let _ = require('lodash');

class YouTuber {
    constructor() {
        this.youtube = new YouTube();
        this.key = ""
        this.numberOfResults = 1;
    }

    setKey(key) {
        this.key = key;
        this.youtube.setKey(this.key);
    }

    search(string, next) {
        this.youtube.search(string, this.numberOfResults, function (error, result) {
            if (error) {
                console.log(error);
                return next(error);
            }
            console.log("Done searching.");
            return next(null, result);
        });
    }

    parseResults(result, next){
        if(!result.items || result.items.length == 0){
            return next({
                data: "No items found.",
                status: 0
            });
        }
        let items = result.items;
        // console.log(items);
        let output = [];
        for(var i in items){
            let item = items[i];
            // console.log(item);
            if(!(_.has(item, 'id') && _.has(item.id, 'kind') && item.id.kind === "youtube#video"))
                continue;
            let name = "Unknown";
            let description = "No detailed description.";
            let id = "lTN1YbiOSmI";
            let fullLink = "http://www.youtube.com/watch?v=" + id;
            let picture = "http://combiboilersleeds.com/images/default/default-6.jpg";

            //get id
            if(_.has(item.id, 'videoId')){
                id = item.id.videoId;
                fullLink = "http://www.youtube.com/watch?v=" + id;
            }

            if(!_.has(item, 'snippet'))
                continue;
            //get name
            if(_.has(item.snippet, 'title'))
                name = item.snippet.title;
            //get description
            if(_.has(item.snippet, 'description') && item.snippet.description !== "")
                description = item.snippet.description;
            //get picture
            if(_.has(item.snippet, 'thumbnails')){
                if(_.has(item.snippet.thumbnails, 'high')){
                    picture = item.snippet.thumbnails.high.url;
                }
                else if(_.has(item.snippet.thumbnails, 'default')){
                    picture = item.snippet.thumbnails.default.url;
                }
            }
            output.push({
                "name": name,
                "description": description,
                "id": id,
                "url": fullLink,
                "picture": picture
            });
        }
    return next({
        data: output,
        status: 1
    });
    }
}

module.exports = YouTuber;