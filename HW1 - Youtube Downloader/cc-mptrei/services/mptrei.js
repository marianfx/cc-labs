let request = require('request');
let _ = require('lodash');


/**
 * Treats the response from the youtubemp3 API and sends the results formatted forward in line (to the controller)
 * 
 * @param {Error} error 
 * @param {any} response 
 * @param {any} body 
 * @param {Function} next (Error, Object) 
 * @returns 
 */
function treatYoutubeMptreiCall(error, response, body, next) {
    if (!response || response.statusCode != 200) {
        return next(error);
    }

    let jsonbody = {}
    try {
        jsonbody = JSON.parse(body);
    } catch (error) {
        console.log(error);
        return next(new Error("Cannot parse response from the youtube-mp3 API. Possible video too long or YouTube does not allow downloading it."));
    }
    
    let dllink = "#";
    let filesize = "0 bytes";
    let length = "0";

    //get link
    if (_.has(jsonbody, "link"))
        dllink = jsonbody.link;
    //get filesize
    if (_.has(jsonbody, "filesize")) {
        let tempint = parseInt(jsonbody.filesize);
        let MB = parseFloat(tempint / 1024 / 1024).toFixed(2);
        Math.round()
        filesize = MB.toString() + " MB";
    }
    //get length
    if (_.has(jsonbody, "length")) {
        let tempsecs = parseInt(jsonbody.length);
        let tempmins = Math.trunc(tempsecs / 60);
        tempsecs = tempsecs % 60;
        let minsstring = tempmins < 10 ? "0" + tempmins.toString() : tempmins.toString();
        let secsstring = tempsecs < 10 ? "0" + tempsecs.toString() : tempsecs.toString();
        length = minsstring + ":" + secsstring;
    }

    return next(null, {
        "link": dllink,
        "size": filesize,
        "length": length
    });
}


class MPTrei {
    constructor() {
        this.baseurl = "http://www.youtubeinmp3.com/fetch/?format=JSON&&filesize=1&video=@video_link"
    }

    getVideoData(yturl, next) {
        let mpturl = this.baseurl.replace("@video_link", yturl);
        request(mpturl, function(error, response, body){
            treatYoutubeMptreiCall(error, response, body, next);
        });
    }
}

module.exports = MPTrei;