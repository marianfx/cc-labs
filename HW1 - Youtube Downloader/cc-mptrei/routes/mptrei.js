let _ = require("lodash");


/**
 * Calls the MPTrei service, handles it's return and sends the data to the caller.
 * 
 * @param {any} me Is the reference to the MRouter object.
 * @param {any} req Is the Request object.
 * @param {any} res Is the Response Object
 * @returns 
 */
function treatMpTreiCall(me, req, res) {
    if (!req.query["yt"]) {
        return res.send({
            data: "Error: You must specify the youtube link parameter (yt).",
            status: 0
        });
    }
    let yturl = req.query["yt"];
    let message = "Getting download link for video '" + yturl + "'.";
    console.log(message);

    // The MPTrei service calls the youtubemp3 API
    me.mptrei.getVideoData(yturl, function (error, result) {
        if (error || !result) {
            let errMsg = "Cannot get result. Response is null.";
            if(error)
                errMsg = _.has(error, "message")? error.message: error.toString();
            console.log(errMsg);

            return res.send({
                data: "Error. " + errMsg,
                status: 0
            });
        }

        console.log(result);
        return res.send({
            data: result,
            status: 1
        });
    });
}


class MRouter {

    /**
     * Creates an instance of MRouter.
     * @param {express} ex The express app. 
     */
    constructor(ex) {
        this.router = ex.Router();
        let MPTreier = require('../services/mptrei');
        this.mptrei = new MPTreier();
    }


    /**
     * Builds this router's functionality and returns it.
     * @returns {Router}
     */
    buildRouter() {
        let me = this;
        this.router.get("/", function(req, res){
            treatMpTreiCall(me, req, res);
        });

        return this.router;
    }
}

module.exports = MRouter;