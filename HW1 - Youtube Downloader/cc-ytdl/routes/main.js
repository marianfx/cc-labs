let request = require('request');
let config = require('../config');
let swig = require('swig');
let _ = require('lodash');


/**
 * Handles the route /search?q=""[&render=1&count=x]. Requests the YouTube API.
 * 
 * @param {any} req Is the request object.
 * @param {any} res Is the response object.
 */
function treatYouTubeSearch(req, res) {
    if (!req.query["q"] || req.query["q"] === "") {
        return res.send({
            data: "Error: You must specify the search query.",
            status: 0
        });
    }
    let searchQuery = req.query["q"];

    let render = "0";
    if (req.query["render"])
        render = req.query["render"].toString();

    let count = "1";
    if (req.query["count"])
        count = req.query["count"].toString();


    let yturl = config.ytUrl.replace("@search_string", searchQuery).replace("@count", count.toString());
    console.log(yturl);

    return request(yturl, function (error, response, body) {
        treatYouTubeResponse(res, render, error, response, body);
    });
}

/**
 * Treats the response from the local MPTrei API
 * 
 * @param {any} res The Response Object.
 * @param {any} render "1" / "0" - specified whether to return result as a JSON object (list) or a rendered collection.
 * @param {any} error Error object from the request
 * @param {any} response The response object
 * @param {any} body The body of the response
 */
function treatYouTubeResponse(res, render, error, response, body) {
    let resp = {
        data: {},
        errors: {},
        status: 0
    };
    //check for error contacting the API
    if (!response || response.statusCode != 200) {
        console.log("Got error" + error);
        let errMsg = _.has(error, "message") ? error.message : error.toString();
        resp.data = "Error contacting YouTube API. Details: " + errMsg;
        return res.send(resp);
    }

    // check for error from the APi
    let jsonBody = JSON.parse(body);
    let status = jsonBody.status;
    let _data = jsonBody.data;
    if (status != 1) {
        console.log("Got error: " + _data)
        resp.data = "Error from the YT APi. Details: " + _data;
        return res.send(resp);
    }

    //for each song, call the mptrei API to get song links
    callMpTreiSearch(_data, 0, [], [], function (errorList, outList) {
        if (errorList.length != 0) {
            resp.errors = errorList;
            if(outList.length == 0){
                resp.data = errorList[0];
                return res.send(resp);
            }
        }

        resp.status = 1;
        switch (render) {
            case "1":
                resp.data = swig.renderFile("./views/list.swig", {
                    data: outList
                });
                break;

            default:
                resp.data = outList;
                break;
        }
        return res.send(resp);
    });
}


/**
 * Recursive function, for each song data from the 'songs' array calls the MPTrei API to get it's download link and saves it into the 'newOutput' object.
 * 
 * @param {Array} songs The array of song data (returned from the YT API)
 * @param {Number} index The current index (for the recursive calls)
 * @param {Array} newOutput The array containing the new completed data.
 * @param {Array} errorList The array containing the new completed data.
 * @param {Function} next The function to call on finish.
 */
function callMpTreiSearch(songs, index, newOutput, errorList, next) {
    if (index >= songs.length) {
        return next(errorList, newOutput);
    }

    let cSong = songs[index];
    let cError = null;

    treatMpTreiSearchCall(cSong, function (error, newsongdata) {
        if (error) {
            console.log("Got error: " + error);
            cError = "Error for [" + cSong.name + " - " + cSong.url + "]: " + error;
            errorList.push(cError);
        }
        else{
            console.log(newsongdata);
            newOutput.push(newsongdata);
        }
        return callMpTreiSearch(songs, index + 1, newOutput, errorList, next);
    });
}


/**
 * Does a request on the MPTrei API and forwards the formatted results (adds to song data the Download Link, Length and Size)
 * 
 * @param {any} songdata The object (from the YT API) containing the song url
 * @param {any} next The function to call on finish.
 */
function treatMpTreiSearchCall(songdata, next) {
    let generatedError = null;
    let mptreiUrl = config.mptreiUrl.replace("@video_url", songdata.url);

    request(mptreiUrl, function (error, response, body) {
        //check for error contacting the API
        if (!response || response.statusCode != 200) {
            let errMsg = _.has(error, "message") ? error.message : error.toString();
            generatedError = "Error contacting MPTrei API. Details: " + errMsg;
            return next(generatedError);
        }
        // check for error from the APi
        let jsonBody = JSON.parse(body);
        let status = jsonBody.status;
        let _data = jsonBody.data;
        if (status != 1) {
            console.log("Got error: " + _data)
            generatedError = "Error from the YouTube-MP3 API. Details: " + _data;
            return next(generatedError);
        }

        songdata.dllink = _data.link;
        songdata.length = _data.length;
        songdata.size = _data.size;
        return next(null, songdata);
    });
}


class MainRouter {

    /**
     * Creates an instance of MRouter.
     * @param {express} ex The express app. 
     */
    constructor(ex) {
        this.router = ex.Router();
    }


    /**
     * @returns {Router}
     */
    buildRouter() {
        let me = this;
        this.router.get("/search/", treatYouTubeSearch);
        return this.router;
    }
}

module.exports = MainRouter;