﻿$("#submit_btn").click(doSearch);

function doSearch() {
    var _search = $("#search_string").val();
    var _no = $("#count_val").val()
    var _render = $("#get_json").is(":checked")? 0: 1;
    console.log(_render);
    var _count = parseInt(_no);
    var url = "http://localhost:3000/api/search"; //?id=bade8051-f56d-4187-9726-8694c9ca6aee
    var _data = {
        q: _search,
        render: _render,
        count: _count
    };
    $.ajax({
        type: "GET", // type of request
        url: url, //path of the request
        data: _data,
        contentType: "application/x-www-form-urlencoded;odata=verbose", // data content type (header)

        // the function called on Success (no error returned by the server)
        success: function (result) {
            var status = result.status;
            var data = result.data;
            var errMsg = "";
            if (result.status == 0) {
                errMsg = data;
                displaySwal("Error happened", "Check bottom of the page for details", "warning");
            } else {

                if (Array.isArray(result.errors) && result.errors.length != 0) {
                    var errMsg = "";
                    for (var errIndex in result.errors) {
                        errMsg += "<p>" + result.errors[errIndex] + "</p>";
                    }
                    console.log(errMsg);
                    displaySwal("We must show you some warnings", "Check bottom of the page for details", "warning");
                }

                if (Array.isArray(data))
                    $(".collection").text(JSON.stringify(data));
                else
                    $(".collection").html(data);
            }
            $("#errordetails").html(errMsg);
        },
        // the function called on error (error returned from server or TimeOut Expired)
        error: function (err) {

            var errorText = "Unknown error happened.";
            if (err.hasOwnProperty("responseText")) {
                var response = JSON.parse(err.responseText);
                if (response.hasOwnProperty("message")) {
                    errorText = response.message;
                } else if (err.hasOwnProperty("statusText")) {
                    errorText = response.statusText;
                } else {
                    errorText = response.toString();
                }
            }
            console.log(err);
            console.log(errorText);
            return displaySwal("Error happened", errorText, "error");
        },
        timeout: 10000 // the time limit to wait for a response from the server, milliseconds
    });
}

function displaySwal(title, text, type, imageUrl) {

    var swalConfig = {
        title: title,
        text: text,
        showConfirmButton: false,
        showCancelButton: false,
        showLoaderOnConfirm: false,
        html: true
    };

    if (type) {
        swalConfig.type = type;
    }

    if (imageUrl) {
        swalConfig.imageUrl = imageUrl;
        swalConfig.imageSize = '180x180';
    }

    swal(swalConfig);
    setTimeout(function () {
        swal.close();
    }, 3000);
}