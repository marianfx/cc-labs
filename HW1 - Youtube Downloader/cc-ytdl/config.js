
module.exports = {
    appPort: '3000',
    appUrl: "http://localhost:3000",
    ytUrl: "http://localhost:3001?q=@search_string&count=@count",
    mptreiUrl: "http://localhost:3002?yt=@video_url"
};